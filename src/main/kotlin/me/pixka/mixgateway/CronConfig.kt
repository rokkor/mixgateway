package me.pixka.mixgateway

import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.config.CorsRegistry
import org.springframework.web.reactive.config.EnableWebFlux
import org.springframework.web.reactive.config.WebFluxConfigurer

//
//@Configuration
//@EnableWebFlux
//class CorsGlobalConfiguration : WebFluxConfigurer {
//    override fun addCorsMappings(corsRegistry: CorsRegistry) {
//        println("************** Config Cors")
//        corsRegistry.addMapping("/**")
//            .allowedOrigins("*")
//            .allowedMethods("*")
//            .maxAge(3600)
//    }
//}

//
//@Configuration
//class CORSConfiguration : WebFluxConfigurer {
//    override fun addCorsMappings(registry: CorsRegistry) {
//        registry.addMapping("/**")
//            .allowCredentials(true)
//            .allowedOrigins("*")
//            .allowedHeaders("*")
//            .allowedMethods("*")
//            .exposedHeaders(HttpHeaders.SET_COOKIE)
//    }
//
//    @Bean
//    fun corsWebFilter(): CorsWebFilter {
//        println("************************ Config Bean cors ***********************")
//        val corsConfiguration = CorsConfiguration()
//        corsConfiguration.allowCredentials = true
//        corsConfiguration.addAllowedHeader("*")
//        corsConfiguration.addAllowedMethod("*")
//        corsConfiguration.addAllowedOrigin("*")
//        corsConfiguration.addExposedHeader(HttpHeaders.SET_COOKIE)
//        val corsConfigurationSource = UrlBasedCorsConfigurationSource()
//        corsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration)
//        return CorsWebFilter(corsConfigurationSource)
//    }
//}