package me.pixka.mixgateway

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity.AuthorizeExchangeSpec
import org.springframework.security.config.web.server.ServerHttpSecurity.OAuth2ResourceServerSpec
import org.springframework.security.web.server.SecurityWebFilterChain




@Configuration
class SecConfig {

    @Bean
    fun springSecurityFilterChain(serverHttpSecurity: ServerHttpSecurity): SecurityWebFilterChain? {
        serverHttpSecurity
            .csrf().disable()
            .authorizeExchange { exchange: AuthorizeExchangeSpec ->
                exchange.pathMatchers("/eureka/**")
                    .permitAll()
                    .anyExchange()
                    .authenticated()
            }
            .oauth2ResourceServer().jwt()
        serverHttpSecurity.cors()
        return serverHttpSecurity.build()
    }
}